# oxre

oxre is an abbreviation and stands for "Ox, remind", Ox being a play on cowsay and simply being put together with "remind".
It's meant as a simple shell script for a "to do" list in your terminal using cowsay. (Yes, cowsay is required for it to work)


Tag | Features
------------ | -------------
-h | Display a set of possible tags
-w | Enter write mode to add a reminder
-d | Enter delete mode to remove a specific reminder

It's additionally recommended to either put the script in a directory which is included in the `$PATH`, such as `/local/usr/bin/` or just set an alias in your shell config (e.g. `.bashrc` or `.zshrc`). An example for an alias would be `alias oxre='~/Downloads/oxre/./oxre'`.